from multi_rake import Rake
from unicodedata import normalize
import json



def extract_keywords(document):
   rake = Rake()
   keywords = rake.apply(document)
   return dict(keywords[:5])

def keywords_score(search, doc_keywords):
   search_normalized = normalize('NFD', search)
    
   return sum(tuple(search_normalized.count(word)*doc_keywords[word] for word in doc_keywords))

def keywords_sorter(item):
    return (item[1], item[2])

def keywords_ranking(search, lists):
   ranking = []
   matches = 0

   for l in lists:
      #count how many keywords match
      for word in lists[l]:

         if word in search:
            matches += 1
            print(word)

      ranking.append((l, matches, keywords_score(search, lists[l])))

   return sorted(ranking, key=keywords_sorter)
