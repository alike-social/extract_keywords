from word_extractor import *

doc1 =  """
En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor. Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados, lentejas los viernes, algún palomino de añadidura los domingos, consumían las tres partes de su hacienda. El resto della concluían sayo de velarte, calzas de velludo para las fiestas con sus pantuflos de lo mismo, los días de entre semana se honraba con su vellori de lo más fino. Tenía en su casa una ama que pasaba de los cuarenta, y una sobrina que no llegaba a los veinte, y un mozo de campo y plaza, que así ensillaba el rocín como tomaba la podadera.
"""

doc2 = """
Era un día luminoso y frío de abril y los relojes daban las trece. Winston Smith, con la barbilla clavada en el pecho en su esfuerzo por burlar el molestísimo viento, se deslizó rápidamente por entre las puertas de cristal de las Casas de la Victoria, aunque no con la suficiente rapidez para evitar que una ráfaga polvorienta se colara con él.

El vestíbulo olía a legumbres cocidas y a esteras viejas. Al fondo, un cartel de colores, demasiado grande para hallarse en un interior, estaba pegado a la pared. Representaba sólo un enorme rostro de más de un metro de anchura: la cara de un hombre de unos cuarenta y cinco años con un gran bigote negro y facciones hermosas y endurecidas. 
"""

keywords_doc1 = extract_keywords(doc1)
keywords_doc2 = extract_keywords(doc2)

#print(keywords_doc1)
#print(keywords_doc2)
#print(keywords_score("quebrantos y fiestas fiestas", keywords_doc1))
#print(keywords_score("quebrantos y fiestas fiestas", keywords_doc2))
lista1 = {"id_lista1":keywords_doc1, "id_lista2":keywords_doc2}


print(keywords_ranking("viento y barbilla , barbilla", lista1))
